# lca-android-ui

## Purpose

An Android implementation for a lap counter app. This app serves two purposes:

1) Allow people to easily track and monitor how many laps they walk on
a given track.  This is especially useful for people enjoying a Viking
River Cruise in Europe.

2) Illustrate a simple, clean, multi-platform, multi-module Android app employing modern JetPack
libraries and following the principles and practices of Clean Code espoused by Robert (Uncle Bob)
Martin.

## Audience

A typical User will employ the app by starting a walking session on the app, putting the phone
running the app in a pocket and pressing the volume up button for each lap walked. Future
enhancements may simplify this by adding motion detection features to the app.

A typical developer will work in one of three modules: the Android UI (this module) using Kotlin
and Android Studio, an iOS UI with Swift and XCode, or a common section of code using Kotlin and
IntelliJ.

## Workflow

Typical workflow for contributors is:

1) get an understanding of a feature, bug fix or enhancement by entering a description into the
issues section of the (GitLab) repository,

2) check out the relevant modules into a named, persisted branch,

3) for each module, update the version information, README.md (this one) and ReleaseNotes.md files
as appropriate for the intended changes,

4) for each module, write one of more automated unit tests using TDD to validate the code works as
intended and ensures that future changes do not break the one being developed,

5) for each module, run code coverage analysis to ensure that 100% code coverage has been obtained
--- this analysis helps determine what remaining tests need to be written,

6) for each module, run code analysis tools (e.g. lint) to address all warnings and errors that do
not prevent the app from running --- 0 warnings and errors is the goal,

7) for each module, commit and document the source code changes,

8) for each module, publish (deploy) the developed code to the source code repository,

9) after review by the project maintainer, or a delegated colleague, changes may be requested,

10) after the code is merged to the master branch, the maintainer (or a delegate) will publish
the changes to the relevant destination, Play Store for Android, App Store for iOS, and close the
original issue.

### Developer setup

Since this app is multi-platform (Android and iOS) there are three modules:

1) Android UI (Android Studio is recommended IDE)
2) iOS UI (XCode is recommended IDE)
3) Common Core (IntelliJ is recommended IDE)

Typically, two of the three are worked on simultaneously. Sometimes all three.

The common core module must be built and deployed to a local maven repository where it will be found
by Gradle as an implementation dependency.
