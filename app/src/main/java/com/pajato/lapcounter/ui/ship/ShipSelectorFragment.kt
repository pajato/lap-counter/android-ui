package com.pajato.lapcounter.ui.ship

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil.inflate
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.pajato.lapcounter.ui.R
import com.pajato.lapcounter.ui.databinding.FragmentShipBinding

class ShipSelectorFragment : Fragment() {

    private lateinit var binding: FragmentShipBinding
    private val shipSelectorViewModel by lazy {
        ViewModelProvider(this).get(ShipSelectorViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = inflate(inflater, R.layout.fragment_ship, container, false)
        val navController = this.findNavController()
        val adapter = ShipSelectorAdapter(
            inflater,
            shipSelectorViewModel,
            navController
        )
        fun setupRecyclerView() = binding.categoryList.let { recyclerView ->
            recyclerView.adapter = adapter
            recyclerView.layoutManager = LinearLayoutManager(context)
        }

        binding.lifecycleOwner = this
        binding.model = shipSelectorViewModel
        setupRecyclerView()
        return binding.root
    }

}
