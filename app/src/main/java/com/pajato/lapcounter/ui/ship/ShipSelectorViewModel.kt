package com.pajato.lapcounter.ui.ship

import android.view.View
import androidx.lifecycle.ViewModel
import com.pajato.lapcounter.ui.ItemSelectorViewModel
import com.pajato.lapcounter.ui.MainActivity
import com.pajato.lapcounter.core.SelectorItem

class ShipSelectorViewModel : ViewModel(),
    ItemSelectorViewModel {
    private val shipSelector by lazy { MainActivity.shipSelector }
    override lateinit var item: SelectorItem

    override fun getCategoryName(): String = if (shipSelector.isSelectable(item)) "" else item.name

    override fun getCategoryContentDescription() = "Category ${getCategoryName()}"

    override fun getCategoryVisibility(): Int =
        if (shipSelector.isSelectable(item)) View.GONE else View.VISIBLE

    override fun getItemContentDescription() = "Ship ${item.name}"

    override fun getItemList(): List<SelectorItem> = shipSelector.list

    override fun getItemName(): String = item.name

    override fun getItemVisibility(): Int =
        if (shipSelector.isSelectable(item)) View.VISIBLE else View.GONE

    override fun setSelectedItem(item: SelectorItem) {
        shipSelector.selectedItem = item
    }

}
