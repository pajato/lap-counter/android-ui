package com.pajato.lapcounter.ui

import android.view.View
import androidx.navigation.NavController
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_LONG
import com.google.android.material.snackbar.Snackbar
import com.pajato.lapcounter.core.CategoryItem
import com.pajato.lapcounter.core.SelectorItem

fun handleItemContainerClick(
    item: SelectorItem,
    itemContainer: View,
    actionResource: Int,
    messageResource: Int,
    navController: NavController,
    model: ItemSelectorViewModel
) {
    fun showWarningOnCategorySelection() {
        val text = itemContainer.context.getString(messageResource)
        Snackbar.make(itemContainer, text, LENGTH_LONG).show()
    }
    fun selectItemAndNavigateHome() {
        model.setSelectedItem(item)
        navController.navigate(actionResource)
    }

    if (item is CategoryItem) showWarningOnCategorySelection() else selectItemAndNavigateHome()

}
