package com.pajato.lapcounter.ui.session

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil.inflate
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.pajato.lapcounter.ui.databinding.FragmentSessionBinding
import com.pajato.lapcounter.ui.MainActivity
import com.pajato.lapcounter.ui.MainActivity.PersistenceHelper.cruiseSelector
import com.pajato.lapcounter.ui.MainActivity.PersistenceHelper.lapCounter
import com.pajato.lapcounter.ui.MainActivity.PersistenceHelper.shipSelector
import com.pajato.lapcounter.ui.R.layout.fragment_session

class SessionFragment : Fragment() {
    internal lateinit var binding: FragmentSessionBinding
    private val sessionViewModel by lazy {
        ViewModelProvider(this).get(SessionViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fun setSessionFragment() {
            val currentActivity = activity as MainActivity
            currentActivity.setSessionFragment(this)
        }

        fun setupBindingVariables() {
            fun bindVariables() {
                binding.model = sessionViewModel
                binding.countLapButton.setOnClickListener { updateLapCount() }
                binding.currentSessionInfo = lapCounter.getCurrentSessionInfo()
            }

            binding = inflate(inflater, fragment_session, container, false)
            bindVariables()
            updateUI()
        }

        setSessionFragment()
        lapCounter.startSession(cruiseSelector.selectedItem.name, shipSelector.selectedItem.name)
        setupBindingVariables()
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        val currentActivity = activity as MainActivity
        currentActivity.acceptVolumeKeyPresses = true
    }

    override fun onStop() {
        super.onStop()
        val currentActivity = activity as MainActivity
        currentActivity.acceptVolumeKeyPresses = false
    }

    internal fun updateUI() {
        val lapCount = lapCounter.getCurrentSessionInfo().lapCountAsString
        val initialSessionVisibility = if (lapCount == "0") View.VISIBLE else View.GONE
        val currentSessionVisibility = if (lapCount != "0") View.VISIBLE else View.GONE

        binding.currentLapCount.visibility = currentSessionVisibility
        binding.currentDistance.visibility = currentSessionVisibility
        binding.initialSessionPrompt.visibility = initialSessionVisibility
    }

}

fun SessionFragment.updateLapCount() {
    lapCounter.countLap()
    this.binding.currentSessionInfo = lapCounter.getCurrentSessionInfo()
    this.updateUI()
}
