package com.pajato.lapcounter.ui.cruise

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil.inflate
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.pajato.lapcounter.ui.R
import com.pajato.lapcounter.ui.databinding.FragmentCruiseBinding

class CruiseSelectorFragment : Fragment() {

    private lateinit var binding: FragmentCruiseBinding
    private val cruiseSelectorViewModel by lazy {
        ViewModelProvider(this).get(CruiseSelectorViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = inflate(inflater, R.layout.fragment_cruise, container, false)
        val navController = this.findNavController()
        val adapter = CruiseSelectorAdapter(
            inflater,
            cruiseSelectorViewModel,
            navController
        )
        fun setupRecyclerView() = binding.categoryList.let { recyclerView ->
            recyclerView.adapter = adapter
            recyclerView.layoutManager = LinearLayoutManager(context)
        }

        binding.lifecycleOwner = this
        binding.model = cruiseSelectorViewModel
        setupRecyclerView()
        return binding.root
    }

}
