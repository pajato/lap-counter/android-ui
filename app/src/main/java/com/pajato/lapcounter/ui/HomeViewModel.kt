package com.pajato.lapcounter.ui

import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.pajato.lapcounter.core.SessionInfo

class HomeViewModel : ViewModel() {
    val cruiseSelector by lazy { MainActivity.cruiseSelector }
    val shipSelector by lazy { MainActivity.shipSelector }
    private val lapCounter by lazy { MainActivity.lapCounter }

    val cumulativeSessionInfo: SessionInfo
        get() = lapCounter.getCumulativeSessionInfo()

    fun getSelectedCruiseDescription() = cruiseSelector.getDescription(cruiseSelector.selectedItem)

    fun checkShipAndCruiseSelections(
        controller: NavController,
        cruiseSelectAction: Int,
        shipSelectAction: Int
    ) {
        val needsCruiseSelected = cruiseSelector.selectedItem.name.isEmpty()
        val needsShipSelected = shipSelector.selectedItem.name.isEmpty()

        when {
            needsCruiseSelected -> controller.navigate(cruiseSelectAction)
            needsShipSelected -> controller.navigate(shipSelectAction)
        }
    }
}
