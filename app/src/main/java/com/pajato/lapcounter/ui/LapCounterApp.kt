package com.pajato.lapcounter.ui

import android.app.Application
import com.pajato.lapcounter.core.CruiseSelector
import com.pajato.lapcounter.core.LapCounter
import com.pajato.lapcounter.core.ShipSelector
import org.koin.android.ext.android.startKoin
import org.koin.dsl.module.module

@Suppress("unused")
class LapCounterApp : Application() {
    private val koinModule = module {
        single { (persistenceDir: String) -> LapCounter(persistenceDir) }
        single { (persistenceDir: String) -> ShipSelector(persistenceDir) }
        single { (persistenceDir: String) -> CruiseSelector(persistenceDir) }
    }

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(koinModule))
    }
}
