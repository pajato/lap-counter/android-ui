package com.pajato.lapcounter.ui.ship

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.pajato.lapcounter.ui.ItemSelectorViewModel
import com.pajato.lapcounter.ui.R
import com.pajato.lapcounter.core.CategoryItem
import com.pajato.lapcounter.core.SelectorItem
import com.pajato.lapcounter.ui.databinding.ShipItemBinding
import com.pajato.lapcounter.ui.databinding.ShipItemBinding.inflate
import com.pajato.lapcounter.ui.handleItemContainerClick
import com.pajato.lapcounter.ui.ship.ShipSelectorAdapter.ItemViewHolder

class ShipSelectorAdapter(
    private val inflater: LayoutInflater,
    private val viewModel: ItemSelectorViewModel,
    private val navController: NavController
) : ListAdapter<SelectorItem, ItemViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<SelectorItem>() {
        override fun areItemsTheSame(
            oldItem: SelectorItem,
            newItem: SelectorItem
        ): Boolean = oldItem === newItem

        override fun areContentsTheSame(
            oldItem: SelectorItem,
            newItem: SelectorItem
        ): Boolean = oldItem.name == newItem.name
    }

    private val list = viewModel.getItemList()

    inner class ItemViewHolder(private val binding: ShipItemBinding) : ViewHolder(binding.root) {
        fun bind(position: Int) {
            val item = list[position]
            viewModel.item = item
            binding.model = viewModel
            binding.itemContainer.setOnClickListener {
                handleItemContainerClick(
                    item = item,
                    itemContainer = it,
                    actionResource = R.id.action_shipSelectorFragment_to_homeFragment,
                    messageResource = R.string.ship_select_warning_message,
                    navController = navController,
                    model = viewModel
                )
            }
            binding.categoryName.contentDescription =
                if (item is CategoryItem) viewModel.getCategoryContentDescription() else ""
            binding.itemName.contentDescription =
                if (item is CategoryItem) viewModel.getItemContentDescription() else ""
            binding.executePendingBindings()
        }
    }

    init {
        setHasStableIds(true)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) = holder.bind(position)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ItemViewHolder(inflate(inflater, parent, false))

    override fun getItemCount() = list.size

    override fun getItemId(position: Int): Long = position.toLong()
}
