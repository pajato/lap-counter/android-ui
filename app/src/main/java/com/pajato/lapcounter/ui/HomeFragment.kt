package com.pajato.lapcounter.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil.inflate
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.pajato.lapcounter.ui.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private val homeViewModel by lazy { ViewModelProvider(this).get(HomeViewModel::class.java) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fun setupBindingVariables() {
            fun navigateToSession(view: View) =
                view.findNavController().navigate(R.id.action_homeFragment_to_sessionFragment)

            binding.model = homeViewModel
            binding.startSessionFAB.setOnClickListener { view -> navigateToSession(view) }
        }

        fun handleLapCount() {
            val lapCount = homeViewModel.cumulativeSessionInfo.lapCountAsString
            val initialSessionVisibility = if (lapCount == "0") View.VISIBLE else View.GONE
            val cumulativeSessionVisibility = if (lapCount != "0") View.VISIBLE else View.GONE

            binding.cumulativeLapsIcon.visibility = cumulativeSessionVisibility
            binding.cumulativeLapCount.visibility = cumulativeSessionVisibility
            binding.cumulativeDistance.visibility = cumulativeSessionVisibility
            binding.initialSessionPrompt.visibility = initialSessionVisibility
        }

        binding = inflate(inflater, R.layout.fragment_home, container, false)
        setupBindingVariables()
        handleLapCount()
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        homeViewModel.checkShipAndCruiseSelections(
            this.findNavController(),
            shipSelectAction = R.id.action_homeFragment_to_shipSelectorFragment,
            cruiseSelectAction = R.id.action_homeFragment_to_cruiseSelectorFragment
        )
    }

}
