package com.pajato.lapcounter.ui.cruise

import android.view.View
import androidx.lifecycle.ViewModel
import com.pajato.lapcounter.ui.ItemSelectorViewModel
import com.pajato.lapcounter.ui.MainActivity
import com.pajato.lapcounter.core.CruiseItem
import com.pajato.lapcounter.core.SelectorItem

class CruiseSelectorViewModel : ViewModel(),
    ItemSelectorViewModel {
    private val cruiseSelector by lazy { MainActivity.cruiseSelector }
    override lateinit var item: SelectorItem

    override fun getCategoryContentDescription(): String =
        "Category ${cruiseSelector.getDescription(item)}"

    override fun getCategoryName(): String = "${item.name} River Cruises"

    override fun getCategoryVisibility(): Int =
        if (cruiseSelector.isSelectable(item)) View.GONE else View.VISIBLE

    override fun getItemContentDescription(): String = "Cruise ${item.name}"

    override fun getItemList(): List<SelectorItem> = cruiseSelector.list

    override fun getItemName(): String = item.name

    override fun getItemVisibility(): Int =
        if (cruiseSelector.isSelectable(item)) View.VISIBLE else View.GONE

    override fun setSelectedItem(item: SelectorItem) { cruiseSelector.selectedItem = item }

    fun getItemDescription(): String =
        if (item is CruiseItem) cruiseSelector.getDescription(item) else ""

}
