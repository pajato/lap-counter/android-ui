package com.pajato.lapcounter.ui

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class VLAGlideApp : AppGlideModule()
