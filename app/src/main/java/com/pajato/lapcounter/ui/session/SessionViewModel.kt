package com.pajato.lapcounter.ui.session

import androidx.lifecycle.ViewModel
import com.pajato.lapcounter.ui.MainActivity

class SessionViewModel : ViewModel() {
    val cruiseSelector by lazy { MainActivity.cruiseSelector }
    val shipSelector by lazy { MainActivity.shipSelector }

    fun getSelectedCruiseDescription() = cruiseSelector.getDescription(cruiseSelector.selectedItem)

}
