package com.pajato.lapcounter.ui

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect.DEFAULT_AMPLITUDE
import android.os.VibrationEffect.createOneShot
import android.os.Vibrator
import android.view.KeyEvent
import android.view.KeyEvent.ACTION_DOWN
import android.view.KeyEvent.KEYCODE_VOLUME_UP
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil.setContentView
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.pajato.lapcounter.core.CruiseSelector
import com.pajato.lapcounter.core.LapCounter
import com.pajato.lapcounter.core.ShipSelector
import com.pajato.lapcounter.ui.databinding.ActivityMainBinding
import com.pajato.lapcounter.ui.session.SessionFragment
import com.pajato.lapcounter.ui.session.updateLapCount
import kotlinx.android.extensions.CacheImplementation
import kotlinx.android.extensions.ContainerOptions
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

@ContainerOptions(cache = CacheImplementation.NO_CACHE)
class MainActivity : AppCompatActivity() {
    companion object PersistenceHelper {
        internal lateinit var cruiseSelector: CruiseSelector
        internal lateinit var shipSelector: ShipSelector
        internal lateinit var lapCounter: LapCounter
    }

    internal var acceptVolumeKeyPresses: Boolean = false
    private lateinit var sessionFragment: SessionFragment
    private lateinit var drawerLayout: DrawerLayout
    private val vibrator: Vibrator by lazy {
        getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        fun setupNavigationDrawer(binding: ActivityMainBinding) {
            drawerLayout = binding.drawerLayout
            val navController = findNavController(R.id.lcaNavHostFragment)
            NavigationUI.setupActionBarWithNavController(this, navController, drawerLayout)
            NavigationUI.setupWithNavController(binding.navView, navController)
        }

        fun setupHelper(persistenceDir: String) {
            val cruiseSelector: CruiseSelector by inject { parametersOf(persistenceDir) }
            val shipSelector: ShipSelector by inject { parametersOf(persistenceDir) }
            val lapCounter: LapCounter by inject { parametersOf(persistenceDir) }

            PersistenceHelper.cruiseSelector = cruiseSelector
            PersistenceHelper.shipSelector = shipSelector
            PersistenceHelper.lapCounter = lapCounter
        }

        val binding by lazy { setContentView<ActivityMainBinding>(this, R.layout.activity_main) }

        super.onCreate(savedInstanceState)
        setupNavigationDrawer(binding)
        setupHelper(this.filesDir.absolutePath)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = this.findNavController(R.id.lcaNavHostFragment)
        return NavigationUI.navigateUp(navController, drawerLayout)
    }


    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        val value: Int by lazy {
            when {
                //event == null -> -1
                event.keyCode == KEYCODE_VOLUME_UP && event.action == ACTION_DOWN -> 1
                else -> 0
            }
        }
        fun checkForValidVolumeUpKeyPress(): Boolean {
            fun vibrateDevice() {
                when {
                    Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ->
                        vibrator.vibrate(createOneShot(250, DEFAULT_AMPLITUDE))
                    else -> @Suppress("DEPRECATION") vibrator.vibrate(250)
                }
            }

            fun handleVolumeUpKeyPress() {
                sessionFragment.updateLapCount()
                vibrateDevice()
            }

            if (value > 0) handleVolumeUpKeyPress()
            return true
        }

        return when {
            //event == null -> super.dispatchKeyEvent(event)
            acceptVolumeKeyPresses -> checkForValidVolumeUpKeyPress()
            else -> super.dispatchKeyEvent(event)
        }
    }

    fun setSessionFragment(sessionFragment: SessionFragment) {
        this.sessionFragment = sessionFragment
    }

    fun cruiseClickHandler(view: View) {
        this.findNavController(view.id).navigate(R.id.action_homeFragment_to_cruiseSelectorFragment)
    }

    fun shipClickHandler(view: View) {
        this.findNavController(view.id).navigate(R.id.action_homeFragment_to_shipSelectorFragment)
    }

}
