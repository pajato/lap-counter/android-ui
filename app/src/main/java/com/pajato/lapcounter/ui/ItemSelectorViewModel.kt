package com.pajato.lapcounter.ui

import com.pajato.lapcounter.core.SelectorItem

interface ItemSelectorViewModel {
    fun getCategoryName(): String
    fun getCategoryContentDescription(): String
    fun getCategoryVisibility(): Int

    fun getItemContentDescription(): String
    fun getItemList(): List<SelectorItem>
    fun getItemName(): String
    fun getItemVisibility(): Int

    fun setSelectedItem(item: SelectorItem)

    var item: SelectorItem

}
