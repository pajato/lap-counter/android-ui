package com.pajato.lapcounter.ui

import android.widget.ImageView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.swipeRight
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withContentDescription
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.pajato.lapcounter.core.CruiseSelector
import com.pajato.lapcounter.core.DefaultItem
import com.pajato.lapcounter.core.Selector
import com.pajato.lapcounter.core.ShipSelector
import com.pajato.lapcounter.ui.cruise.CruiseSelectorAdapter
import com.pajato.lapcounter.ui.ship.ShipSelectorAdapter
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

const val DANUBE_RIVER_CRUISES = "Danube River Cruises"
const val GRAND_EUROPEAN_TOUR = "Grand European Tour"
const val TEST_CRUISE_SELECTION_FILE_NAME = "test-cruise-selection"
const val TEST_SHIP_SELECTION_FILE_NAME = "test-ship-selection"
const val A_LONGSHIPS = "Category A"
const val LONGSHIP_ALRUNA = "Alruna"

@RunWith(AndroidJUnit4::class)
class SelectionTest {
    private val context = InstrumentationRegistry.getInstrumentation().targetContext
    private val cruiseSelector = CruiseSelector(context.filesDir.absolutePath,
        TEST_CRUISE_SELECTION_FILE_NAME
    )
    private val shipSelector = ShipSelector(context.filesDir.absolutePath,
        TEST_SHIP_SELECTION_FILE_NAME
    )

    @Before
    fun setUp() {
        loadKoin(cruiseSelector, shipSelector)
    }

    @Test
    fun verify_cruise_selection_fragment_is_displayed() {
        selectNeitherCruiseNorShip {
            assertEquals("", cruiseSelector.selectedItem.name)
            assertEquals("", shipSelector.selectedItem.name)
            onView(withText(DANUBE_RIVER_CRUISES)).check(matches(isDisplayed()))
        }
    }

    @Test
    fun verify_cruise_selection_fragment_has_warning_popup_on_selecting_category() {
        selectNeitherCruiseNorShip {
            onView(withText(DANUBE_RIVER_CRUISES)).perform(click())
            onView(withId(com.google.android.material.R.id.snackbar_text))
                .check(matches(withText(R.string.cruise_select_warning_message)))
            onView(withId(com.google.android.material.R.id.snackbar_text))
                .perform(swipeRight())
            onView(withText(GRAND_EUROPEAN_TOUR)).perform(click())
            onView(withContentDescription(A_LONGSHIPS)).perform(click())
            onView(withId(com.google.android.material.R.id.snackbar_text))
                .check(matches(withText(R.string.ship_select_warning_message)))
            onView(withId(com.google.android.material.R.id.snackbar_text))
                .perform(swipeRight())
        }
    }

    @Test
    fun verify_cruise_selection_fragment_on_selecting_cruise() {
        fun verifySelectGrandEuropeanCruise() {
            onView(withText(DANUBE_RIVER_CRUISES)).perform(click())
            onView(withText(GRAND_EUROPEAN_TOUR)).check(matches(isDisplayed()))
            onView(withText(GRAND_EUROPEAN_TOUR)).perform(click())
            onView(withText(DANUBE_RIVER_CRUISES)).check(doesNotExist())
        }
        fun verifySelectAlrunaLongship() {
            onView(withContentDescription(A_LONGSHIPS)).check(matches(isDisplayed()))
            onView(withText(LONGSHIP_ALRUNA)).check(matches(isDisplayed()))
            onView(withText(LONGSHIP_ALRUNA)).perform(click())
            onView(withContentDescription(A_LONGSHIPS)).check(doesNotExist())
        }
        fun verifyNavigateUpFromHomeFragment() {
            onView(withText(LONGSHIP_ALRUNA)).check(matches(isDisplayed()))
            onView(withText(LONGSHIP_ALRUNA)).perform(click())
            onView(withContentDescription(A_LONGSHIPS)).check(matches(isDisplayed()))
            onView(withContentDescription("Navigate up")).perform(click())
            onView(withContentDescription(A_LONGSHIPS)).check(doesNotExist())
        }
        fun verifyCruiseSelectionFromHomeFragment() {
            onView(withText(GRAND_EUROPEAN_TOUR)).check(matches(isDisplayed()))
            onView(withText(GRAND_EUROPEAN_TOUR)).perform(click())
            onView(withText(DANUBE_RIVER_CRUISES)).check(matches(isDisplayed()))
        }

        selectNeitherCruiseNorShip {
            verifySelectGrandEuropeanCruise()
            verifySelectAlrunaLongship()
            verifyNavigateUpFromHomeFragment()
            verifyCruiseSelectionFromHomeFragment()
        }
    }

    @Test
    fun verifyCruiseAndShipSelected() {
        selectCruiseAndShip {
            onView(withId(R.id.home_container)).check(matches(isDisplayed()))
        }
    }

    @Test
    fun verifyCruiseOnlySelected() {
        selectCruise {
            assertEquals(GRAND_EUROPEAN_TOUR, cruiseSelector.selectedItem.name)
            assertEquals("", shipSelector.selectedItem.name)
            onView(withContentDescription(A_LONGSHIPS)).check(matches(isDisplayed()))
        }
    }

    @Test
    fun verifyShipOnlySelected() {
        selectShip {
            assertEquals("", cruiseSelector.selectedItem.name)
            assertEquals(LONGSHIP_ALRUNA, shipSelector.selectedItem.name)
            onView(withText(DANUBE_RIVER_CRUISES)).check(matches(isDisplayed()))
        }
    }

    @Test
    fun verifyDiffCallbackOperations() {
        val item = DefaultItem()
        assertFalse(ShipSelectorAdapter.areItemsTheSame(DefaultItem(), DefaultItem()))
        assertTrue(ShipSelectorAdapter.areItemsTheSame(item, item))
        assertTrue(ShipSelectorAdapter.areContentsTheSame(DefaultItem(), DefaultItem()))
        assertFalse(CruiseSelectorAdapter.areItemsTheSame(DefaultItem(), DefaultItem()))
        assertTrue(CruiseSelectorAdapter.areItemsTheSame(item, item))
        assertTrue(CruiseSelectorAdapter.areContentsTheSame(DefaultItem(), DefaultItem()))
    }

    @Test
    fun exerciseBindImageForNull() {
        fun checkBindImage(scenario: ActivityScenario<MainActivity>) {
            onView(withId(R.id.cruise_image)).check(matches(isDisplayed()))
            scenario.onActivity {
                val view: ImageView = it.findViewById(R.id.cruise_image)
                bindImage(view, null)
            } ?: fail("No scenario!")
        }

        cruiseSelector.selectItem(GRAND_EUROPEAN_TOUR)
        shipSelector.selectItem(LONGSHIP_ALRUNA)
        when (val result = getScenario()) {
            is ScenarioSuccess -> checkBindImage(result.scenario)
            is ScenarioFailure -> reportFailure(
                result.exception
            )
        }
    }

    private fun selectNeitherCruiseNorShip(runner: () -> Unit) {
        fun resetSelectedItems() {
            cruiseSelector.selectedItem = DefaultItem()
            shipSelector.selectedItem = DefaultItem()
        }

        resetSelectedItems()
        runTests(runner)
    }

    private fun selectCruise(runner: () -> Unit) {
        fun setCruiseOnly() {
            shipSelector.selectedItem = DefaultItem()
            cruiseSelector.selectItem(GRAND_EUROPEAN_TOUR)
        }

        setCruiseOnly()
        runTests(runner)
    }

    private fun selectShip(runner: () -> Unit) {
        fun setShipOnly() {
            cruiseSelector.selectedItem = DefaultItem()
            shipSelector.selectItem(LONGSHIP_ALRUNA)
        }

        setShipOnly()
        runTests(runner)
    }

    private fun selectCruiseAndShip(runner: () -> Unit) {
        cruiseSelector.selectItem(GRAND_EUROPEAN_TOUR)
        shipSelector.selectItem(LONGSHIP_ALRUNA)
        runTests(runner)
    }

    private fun runTests(runner: () -> Unit) {
        when (val result = getScenario()) {
            is ScenarioSuccess -> runTests(
                result.scenario,
                runner
            )
            is ScenarioFailure -> reportFailure(
                result.exception
            )
        }
    }
}

fun Selector.selectItem(name: String) {
    val index = this.getItemIndexFromName(name)
    val item = this.list[index]
    this.selectedItem = item

}
