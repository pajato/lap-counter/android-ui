package com.pajato.lapcounter.ui

import android.view.KeyEvent
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.pressKey
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.Visibility.GONE
import androidx.test.espresso.matcher.ViewMatchers.Visibility.VISIBLE
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withContentDescription
import androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.pajato.lapcounter.ui.R.id.count_lap_button
import com.pajato.lapcounter.ui.R.id.current_lap_count
import com.pajato.lapcounter.ui.R.id.initial_session_prompt
import com.pajato.lapcounter.ui.R.id.startSessionFAB
import com.pajato.lapcounter.core.CruiseSelector
import com.pajato.lapcounter.core.LapCounter
import com.pajato.lapcounter.core.ShipSelector
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext.loadKoinModules
import java.io.File

const val TEST_SESSION_FILE_NAME = "test-sessions"

@RunWith(AndroidJUnit4::class)
class SessionTest {
    private val dir = InstrumentationRegistry.getInstrumentation().targetContext.filesDir
    private val cruiseSelector = CruiseSelector(dir.absolutePath,
        TEST_CRUISE_SELECTION_FILE_NAME
    )
    private val shipSelector = ShipSelector(dir.absolutePath,
        TEST_SHIP_SELECTION_FILE_NAME
    )
    private lateinit var lapCounter: LapCounter

    @Before
    fun setUp() {
        val sessionFile = File(dir.absolutePath,
            TEST_SESSION_FILE_NAME
        )
        sessionFile.delete()
        lapCounter = LapCounter(dir.absolutePath,
            TEST_SESSION_FILE_NAME
        )
        loadKoinModules(module {
            single(override = true) { cruiseSelector }
            single(override = true) { shipSelector }
            single(override = true) { lapCounter }
        })
    }

    @Test
    fun when_a_session_is_started_verify_the_session_fragment_is_visible() {
        cruiseSelector.selectItem(GRAND_EUROPEAN_TOUR)
        shipSelector.selectItem(LONGSHIP_ALRUNA)
        val scenario = getScenario()
        scenario.apply {
            onView(withId(startSessionFAB)).check(matches(isDisplayed()))
            onView(withId(startSessionFAB)).perform(click())
            onView(withId(R.id.session_image)).check(matches(isDisplayed()))
        }

    }

    @Test
    fun when_a_session_is_started_verify_correct_message_is_shown() {
        cruiseSelector.selectItem(GRAND_EUROPEAN_TOUR)
        shipSelector.selectItem(LONGSHIP_ALRUNA)
        val scenario = getScenario()
        scenario.apply {
            onView(withId(startSessionFAB)).check(matches(isDisplayed()))
            onView(withId(startSessionFAB)).perform(click())
            onView(withId(initial_session_prompt)).check(matches(withEffectiveVisibility(VISIBLE)))
            onView(withId(current_lap_count)).check(matches(withEffectiveVisibility(GONE)))
            onView(withId(count_lap_button)).check(matches(isDisplayed()))
            onView(withId(count_lap_button)).perform(click())
            onView(withId(initial_session_prompt)).check(matches(withEffectiveVisibility(GONE)))
            onView(withId(current_lap_count)).check(matches(withEffectiveVisibility(VISIBLE)))
            onView(withContentDescription("Navigate up")).perform(click())
            onView(withId(current_lap_count)).check(doesNotExist())
        }

    }

    @Test
    fun when_the_volume_up_key_is_pressed_verify_count_is_incremented() {
        cruiseSelector.selectItem(GRAND_EUROPEAN_TOUR)
        shipSelector.selectItem(LONGSHIP_ALRUNA)
        val scenario = getScenario()
        scenario.apply {
            onView(withId(startSessionFAB)).perform(pressKey(KeyEvent.KEYCODE_A))
            onView(withId(startSessionFAB)).perform(click())
            onView(withId(count_lap_button)).perform(pressKey(KeyEvent.KEYCODE_VOLUME_UP))
            onView(withId(current_lap_count)).check(matches(withEffectiveVisibility(VISIBLE)))
            onView(withId(count_lap_button)).perform(pressKey(KeyEvent.KEYCODE_VOLUME_DOWN))
            onView(withId(current_lap_count)).check(matches(withEffectiveVisibility(VISIBLE)))
        }
    }

    private fun getScenario(): ActivityScenario<MainActivity>? =
        try {
            ActivityScenario.launch(MainActivity::class.java)
        } catch (exc: Exception) {
            fail("Could not get an activity because: \n${exc.message}")
            null
        }

}
