package com.pajato.lapcounter.ui

import androidx.test.core.app.ActivityScenario
import com.pajato.lapcounter.core.CruiseSelector
import com.pajato.lapcounter.core.ShipSelector
import org.junit.Assert.fail
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext.loadKoinModules

sealed class ScenarioResult
class ScenarioSuccess(val scenario: ActivityScenario<MainActivity>) : ScenarioResult()
class ScenarioFailure(val exception: Exception) : ScenarioResult()

fun getScenario(): ScenarioResult =
    try {
        ScenarioSuccess(
            ActivityScenario.launch(
                MainActivity::class.java
            )
        )
    } catch (exc: Exception) {
        ScenarioFailure(exc)
    }

fun runTests(scenario: ActivityScenario<MainActivity>, runner: () -> Unit) {
    scenario.apply {
        runner()
        close()
    }
}

fun reportFailure(exc: Exception) {
    val message = exc.message
    fail("Failed to obtain a valid scenario! Failed with exception message: $message")
}

fun loadKoin(cruiseSelector: CruiseSelector, shipSelector: ShipSelector) {
    loadKoinModules(module {
        single(override = true) { cruiseSelector }
        single(override = true) { shipSelector }
    })
}
