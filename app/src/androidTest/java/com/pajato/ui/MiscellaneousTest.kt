package com.pajato.lapcounter.ui

import androidx.test.core.app.ActivityScenario
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.pajato.lapcounter.core.DefaultItem
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MiscellaneousTest {

    @Test
    fun when_application_starts_selection_verify_state() {
        fun run(scenario: ActivityScenario<MainActivity>) =
            runTests(scenario) {
                MainActivity.cruiseSelector.selectedItem = DefaultItem()
                MainActivity.shipSelector.selectedItem = DefaultItem()
                assertTrue(MainActivity.cruiseSelector.selectedItem.name.isEmpty())
                assertTrue(MainActivity.shipSelector.selectedItem.name.isEmpty())
            }

        when (val result = getScenario()) {
            is ScenarioSuccess -> run(result.scenario)
            is ScenarioFailure -> reportFailure(
                result.exception
            )
        }
    }

    @Test
    fun when_main_activity_is_ready_verify_accept_volume_key_press() {
        fun getAcceptVolumeUpKeyPresses(scenario: ActivityScenario<MainActivity>): Boolean {
            var result = false
            scenario.onActivity { result = it.acceptVolumeKeyPresses }
            return result
        }

        when(val result = getScenario()) {
            is ScenarioSuccess -> assertFalse(getAcceptVolumeUpKeyPresses(result.scenario))
            is ScenarioFailure -> reportFailure(
                result.exception
            )
        }
    }
}
