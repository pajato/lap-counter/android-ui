package com.pajato.lapcounter.ui

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.NavigationViewActions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withContentDescription
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.pajato.lapcounter.ui.R.id.aboutFragment
import com.pajato.lapcounter.ui.R.id.aboutImage
import com.pajato.lapcounter.ui.R.id.navView
import com.pajato.lapcounter.core.CruiseSelector
import com.pajato.lapcounter.core.ShipSelector
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AboutTest {
    private val context = InstrumentationRegistry.getInstrumentation().targetContext
    private val cruiseSelector = CruiseSelector(context.filesDir.absolutePath,
        TEST_CRUISE_SELECTION_FILE_NAME
    )
    private val shipSelector = ShipSelector(context.filesDir.absolutePath,
        TEST_SHIP_SELECTION_FILE_NAME
    )

    @Before
    fun setUp() {
        loadKoin(cruiseSelector, shipSelector)
    }

    @Test
    fun verify_about_fragment() {
        fun run(scenario: ActivityScenario<MainActivity>) =
            runTests(scenario) {
                onView(withContentDescription("Open navigation drawer")).perform(click())
                onView(withId(navView)).perform(NavigationViewActions.navigateTo(aboutFragment))
                onView(withId(aboutImage)).check(matches(isDisplayed()))
            }

        cruiseSelector.selectItem(GRAND_EUROPEAN_TOUR)
        shipSelector.selectItem(LONGSHIP_ALRUNA)
        when (val result = getScenario()) {
            is ScenarioSuccess -> run(result.scenario)
            is ScenarioFailure -> reportFailure(
                result.exception
            )
        }
    }

}
